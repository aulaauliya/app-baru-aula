import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import React, {Component} from 'react';

class HomeS extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warnaText: '#000', //hitam
      inputPertama: '',
      inputKedua: '',
      fullText: '',
    };
  };

  gabungData = () => {
    let inputPertama = this.state.inputPertama; 
    let inputKedua = this.state.inputKedua;
    this.setState({fullText: inputPertama + " " + inputKedua})
  };

  // ubahWarna = () => {
  //   const warna = this.state.warnaText;
  //   if (warna === 'black') {
  //     this.setState({warnaText: 'red'});mm
  //   } else if (warna === 'red') {
  //     this.setState({warnaText: 'blue'});
  //   } else if (warna === 'blue') {
  //     this.setState({warnaText: 'green'});
  //   } else {
  //     this.setState({warnaText: 'black'});
  //   };
  // };


  render = () => {
    return (
      <View style={styles.container}>
        <TextInput style={styles.input} placeholder="Nama Pertama" onChangeText={inputPertama => this.setState({inputPertama})}/>
        <TextInput style={styles.input} placeholder="Nama Kedua"onChangeText={inputKedua => this.setState({inputKedua})}/>
        <Text style={styles.fulltext}>Full Text : {this.state.fullText}</Text>
        <TouchableOpacity onPress={() => this.gabungData()} style={styles.btn}>
          <Text style={{ color: '#000' }}>tekan</Text>
        </TouchableOpacity>
      </View>
    );
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: 'yellow', //putih
  },
  btn: {
    backgroundColor: '#fff', //putih
    padding: 10,
    margin: 10,
  },
  input: {
    backgroundColor: '#fff', //putih
    margin: 15,
    width: '70%',
    paddingLeft: 25,
  },
  fulltext: {
    marginTop: -5,
    color: '#000'//deepping
  }
});

export default HomeS;